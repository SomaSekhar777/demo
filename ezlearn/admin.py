from django.contrib import admin
from django.contrib.auth.models import Permission
from .models import learners, developer, Submission_quiz
from .models import Course, Choice, Question, learnings ,Quiz,Payment


# Register your models here.
admin.site.register([learners, developer,Submission_quiz, Course, learnings, Quiz, Question, Choice,Payment, Permission])
