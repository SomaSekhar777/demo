from django.apps import AppConfig


class EzlearnConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ezlearn'
