import pyotp
from django.core.mail import send_mail
from datetime import datetime, timedelta

def send_otp(request,mail_):
    potp = pyotp.TOTP(pyotp.random_base32(), interval=60)
    otp = potp.now()
    request.session['otp_secret_key'] = potp.secret
    valid_date = datetime.now() + timedelta(minutes=2)
    request.session['otp_valid_date'] = str(valid_date)
    print(otp)
    res = send_mail("hello somu", f'your one time password is {otp}', "somasekhar_devisetty3@gmail.com",[mail_])