# Generated by Django 4.2.2 on 2023-07-19 16:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ezlearn', '0013_alter_learners_profile_picture'),
    ]

    operations = [
        migrations.AlterField(
            model_name='learners',
            name='profile_picture',
            field=models.ImageField(null=True, upload_to='images/'),
        ),
    ]
