# Generated by Django 4.2.2 on 2023-07-15 08:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ezlearn', '0005_course_quiz_question_learnings_choice'),
    ]

    operations = [
        migrations.AlterField(
            model_name='learners',
            name='profile_picture',
            field=models.ImageField(blank=True, default='static/dummy.png', null=True, upload_to='images/'),
        ),
    ]
