# Generated by Django 4.2.2 on 2023-07-13 10:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ezlearn', '0003_alter_developer_developer_name_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='learners',
            name='DOB',
            field=models.DateField(default='2001-01-01', null=True),
        ),
    ]
